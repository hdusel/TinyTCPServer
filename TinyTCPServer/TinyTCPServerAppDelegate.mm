/*
 File: TinyTCPServerAppDelegate.mm
 
 Created by Dusel Hans-Peter on 07.08.11.
 
 License: Open Source Initiative OSI - The MIT License (MIT):Licensing The MIT License (MIT)
 http://www.opensource.org/licenses/MIT
 
 Copyright (c) 2011 Tangerine-Software, Hans-Peter Dusel, hdusel@tangerine-soft.de
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "TinyTCPServerAppDelegate.h"
#import <Foundation/Foundation.h>

@implementation TinyTCPServerAppDelegate

@synthesize window;
@synthesize port = _port;
@synthesize receivedString = _receivedString;

- (id)init
{
    self = [super init];
    if (self)
    {
        _port = 50000;
        _receivedString = nil;
    }
    return self;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    server = [[TANTCPServer alloc] initWithPort:_port];
    [server setDelegate:self];
    
    [self RunServerChanged:runServerButton];
}

-(void)updateTextField:(NSString*)newPortionString
{
    _receivedString = _receivedString ? [NSString stringWithFormat:@"%@\n%@", _receivedString, newPortionString] : newPortionString;
    [self setValue:_receivedString forKey:@"receivedString"];
}

-(void)catchDataOnSocket:(TANTCPSocket*)inSocket
{
    NSAutoreleasePool *arp = [NSAutoreleasePool new];
    for(;;)
    {
        NSMutableString *s = [NSMutableString new];
        char lastChar='\0';
        for (;;)
        {
            NSData *data = [inSocket receiveDataOfSize:1];
            if (data == nil)
            {
                return;
            }
            
            const char thisChar(*(char*)[data bytes]);
            if (thisChar == '\n')
            {
                continue;
            }
            if (thisChar == '\r')
            {
                break;
            }
            [s appendString:[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]];
            lastChar = thisChar;
        }
        
        [self updateTextField:s];
        
        [s release];
    }
    [arp release];
}

-(void)connectRequestOnSocket:(TANTCPSocket*)inSocket
{
//    NSLog(@"**** connectRequestOnSocket called");
    @try 
    {
        [inSocket sendString:@"Hello Server!\n"];
        [NSThread detachNewThreadSelector:@selector(catchDataOnSocket:) toTarget:self withObject:inSocket];
    }
    @catch (NSException *exception) 
    {
        NSLog(@"Exception caught!");
    }
}

-(void)startListenSocketOnServer:(TANTCPServer*)inServer
{
    NSLog(@"**** startListenSocketOnServer called");
}

-(void)stopListenSocketOnServer:(TANTCPServer*)inServer
{
    NSLog(@"**** stopListenSocketOnServer called");
}

- (IBAction)RunServerChanged:(NSButton*)sender 
{
    if (NSOnState == [sender state])
    {
        [server start];
    }
    else
    {
        [server stop];
    }
}
@end
