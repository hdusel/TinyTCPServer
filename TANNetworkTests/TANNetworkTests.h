//
//  TANNetworkTests.h
//  TANNetworkTests
//
//  Created by Dusel Hans-Peter on 08.08.11.
//  Copyright 2011 Tangerine Software. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <TANNetwork/TANNetwork.h>

@interface TANNetworkTests : SenTestCase<TANTCPServerDelegate>

@end
