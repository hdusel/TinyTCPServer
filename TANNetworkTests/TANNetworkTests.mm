//
//  TANNetworkTests.m
//  TANNetworkTests
//
//  Created by Dusel Hans-Peter on 08.08.11.
//  Copyright 2011 Tangerine Software. All rights reserved.
//

#import "TANNetworkTests.h"
#import <netinet/in.h>

@implementation TANNetworkTests

static int s_RequestGot = 0;
static int s_StartListenGot = 0;
static int s_EndListenGot = 0;

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
        // Create a server
    TANTCPServer * server = [[TANTCPServer alloc] initWithPort:54321];
    STAssertNotNil(server, @"server is nil!");
    
    [server setDelegate:self];

    STAssertEquals(0, s_RequestGot, @"");
    STAssertEquals(0, s_StartListenGot, @"");
    STAssertEquals(0, s_EndListenGot, @"");
    
    STAssertTrue(NO == [server isRunning], @"Server is running!");

    [server start];
    
    STAssertTrue(YES == [server isRunning], @"Server is not running!");
    STAssertEquals(0, s_RequestGot, @"");
    STAssertEquals(1, s_StartListenGot, @"");
    STAssertEquals(0, s_EndListenGot, @"");
    
    // Perform a connection
    TANTCPSocket *socket;
    socket = [TANTCPSocket new];
    [socket connectToPeer:INADDR_ANY usingPort:54321];
    usleep(1000*100);
    STAssertEquals(1, s_RequestGot, @"");
    STAssertEquals(1, s_StartListenGot, @"");
    STAssertEquals(0, s_EndListenGot, @"");
    [socket release];
    
    socket = [TANTCPSocket new];
    [socket connectToPeer:INADDR_ANY usingPort:54321];
    usleep(1000*100);
    STAssertEquals(2, s_RequestGot, @"");
    STAssertEquals(1, s_StartListenGot, @"");
    STAssertEquals(0, s_EndListenGot, @"");
    [socket release];

    [server stop];
    [server release];
    STAssertEquals(1, s_EndListenGot, @"");
}

-(void)connectRequestOnSocket:(TANTCPSocket*)inSocket
{
    ++s_RequestGot;
}

-(void)startListenSocketOnServer:(TANTCPServer*)inServer
{
    ++s_StartListenGot;
}

-(void)stopListenSocketOnServer:(TANTCPServer*)inServer;
{
    ++s_EndListenGot;
}
@end
