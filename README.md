## Purpose
This project covers a tiny TCP Socket server (TANTCPServer) which may be useful to implement a two way network connection.

## Brief implementation notes
The TCP Socket Server is implemented as a Thread and will run concurrent to the main thread. Whenever the Server detects a connection request a delegation method will be called.

This method gets a TANTCPSocket which may be used to communicate with the peer. The TANTCPSocket class currently is able just to transfer binary blobs which are handed over as NSData objects and Strings.

If the implementation of the peer uses this TANTCPSocket object to establish the connection it will be able to reconstruct the NSData object (or NSString if used) because it will be able to reconstruct the size and the objects contents because of the special encoding of the payload. This encoding is done by honoring the endian of the peer which means that the size tag of the payload is transformed in big endian disregard of the hosts endianess.

It is implemented by using Objective-C++ and is tested on Mac OS X 10.5. Since teh code uses just "elementary" classes of Objective C it will compile for iOS either.

## Testing
In addition This package contains a Unit test which proves the Server by establishing a simple Communication. In addition this illustrates how to write a simple TCP Client/Server app by using this framework.

## Contact
Hans-Peter Dusel, Tangerine-Software, mailto:hdusel@tangerine-soft.de
