/*
 File: TANTCPSocket.mm
 
 Created by Dusel Hans-Peter on 07.08.11.
 
 License: Open Source Initiative OSI - The MIT License (MIT):Licensing The MIT License (MIT)
 http://www.opensource.org/licenses/MIT
 
 Copyright (c) 2011 Tangerine-Software, Hans-Peter Dusel, hdusel@tangerine-soft.de
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#import "TANTCPSocket.h"
#import <Foundation/Foundation.h>
#import <stdint.h>

static void _socketCallBackWrapper(CFSocketRef s, CFSocketCallBackType type, CFDataRef address, const void *data, void *info);
static CFDataRef dataForAddressAndPort(in_addr_t inAddress, in_port_t inPort);

static uint64_t htonll(const uint64_t& inValue);
static uint64_t ntohll(const uint64_t& inValue);

#pragma mark -
@interface TANTCPSocket (Private)
-(BOOL)initSocket;
-(BOOL)initSocketFromNativeHandle:(CFSocketNativeHandle)nativeHandle;

-(BOOL)addToRunLoop;
-(void)removeFromRunLoop;

-(void)handleEventForSocket:(CFSocketRef)s withCallbackType:(CFSocketCallBackType)type forAddress:(CFDataRef)address withData:(const void*)data;
-(BOOL)setReusePort;

@end // @@interface TANTCPSocket (Private)


#pragma mark -
@implementation TANTCPSocket
-(id)init
{
    self = [super init];
    if (self)
    {
        //        _runLoop = (CFRunLoopRef)runLoop;
        _runLoop = ::CFRunLoopGetCurrent();
        if ( ![self initSocket] )
        {
            [self release];
            self = nil;
        }
    }
    return self;
}

-(id)initWithNativeSocket:(CFSocketNativeHandle)nativeHandle
{
    self = [super init];
    if (self)
    {
        //        _runLoop = (CFRunLoopRef)runLoop;
        _runLoop = ::CFRunLoopGetCurrent();
        if ( ![self initSocketFromNativeHandle:nativeHandle] )
        {
            [self release];
            self = nil;
        }
    }
    return self;
}

- (void)dealloc
{
    [self disconnect];
    [self removeFromRunLoop];
    CFRunLoopStop(_runLoop);
    
    if (_socket)
    {
        CFRelease(_socket);
    }
    
    [super dealloc];
}

-(BOOL)sendString:(NSString*)string
{
    return [self sendData:[string dataUsingEncoding:NSUTF8StringEncoding]];
}

-(BOOL)sendData:(NSData*)data
{
    BOOL success = FALSE;
    if (_socket)
    {
        const CFSocketError err(::CFSocketSendData(_socket,
                                                 nil,
                                                 (CFDataRef)data,
                                                 -1));
        success = (kCFSocketSuccess == err);
    }
    return success;
}

-(void)disconnect
{
    if (_socket)
    {
        CFSocketInvalidate(_socket);
    }
}

-(BOOL)isValid
{
    return _socket && CFSocketIsValid(_socket);
}

-(BOOL)bindToIPAddress:(in_addr_t)inAddress withPort:(in_port_t)inPort
{
    BOOL success = NO;
    
    const BOOL reusePortSuccess([self setReusePort]);
    NSAssert(reusePortSuccess, @"Could not set the socket to reuse the port!");
    
    if (reusePortSuccess)
    {
        const CFSocketError sockErr ( ::CFSocketSetAddress(_socket, dataForAddressAndPort(inAddress, inPort)) );
        success = (kCFSocketSuccess == sockErr);

        NSAssert(success, @"Could not bind socket to IP address and port!");
    }
    return success;
}

-(BOOL)connectToPeer:(in_addr_t)inAddress usingPort:(in_port_t)inPort
{
    BOOL success = NO;
    if (_socket)
    {
        const CFSocketError sockErr(CFSocketConnectToAddress(_socket, dataForAddressAndPort(inAddress, inPort), -1));
        success = (kCFSocketSuccess == sockErr);

        //        NSAssert(success, @"Could not connect to IP address and port!");
    }
    return success;
}

-(NSData*)receiveDataOfSize:(size_t)inSize
{
    NSData *data = nil;
    void *tmpDataBuff = ::malloc(inSize);
    NSAssert(tmpDataBuff, @"tmpDataBuff is nil!");
    if (tmpDataBuff)
    {
        const ssize_t rdDataSize ( ::recv(self.nativeHandle, tmpDataBuff, inSize, 0) );

        if (rdDataSize == inSize)
        {
            data = [NSData dataWithBytesNoCopy:tmpDataBuff length:inSize];
        }
    }
    return data;
}

-(void)pushUint8:(UInt8)inValue
{
    [self sendData:[NSData dataWithBytesNoCopy:&inValue length:sizeof(inValue) freeWhenDone:NO]];
}

-(void)pushUint16BE:(UInt16)inValue
{
    inValue = htons(inValue);
    [self sendData:[NSData dataWithBytesNoCopy:&inValue length:sizeof(inValue) freeWhenDone:NO]];
}

-(void)pushUint32BE:(UInt32)inValue
{
    inValue = htonl(inValue);
    [self sendData:[NSData dataWithBytesNoCopy:&inValue length:sizeof(inValue) freeWhenDone:NO]];
}

-(void)pushUint64BE:(UInt64)inValue
{
    inValue = htonll(inValue);
    [self sendData:[NSData dataWithBytesNoCopy:&inValue length:sizeof(inValue) freeWhenDone:NO]];
}

-(void)pushFloatBE:(float)inValue
{
    [self pushUint32BE:*(UInt32*)&inValue];
}

-(void)pushDoubleBE:(double)inValue
{
    [self pushUint64BE:*(UInt64*)&inValue];
}

-(UInt8)popUint8
{
    UInt8 v;
    NSData *data = [self receiveDataOfSize:sizeof(v)];
    if (!data)
    {
        @throw [NSException new];
    }
    v = *(UInt8*)[data bytes];
    return v;
}

-(UInt16)popUint16BE
{
    UInt16 v;
    NSData *data = [self receiveDataOfSize:sizeof(v)];
    if (!data)
    {
        @throw [NSException new];
    }
    v = *(UInt16*)[data bytes];
    return ntohs(v);
}

-(UInt32)popUint32BE
{
    UInt32 v;
    NSData *data = [self receiveDataOfSize:sizeof(v)];
    if (!data)
    {
        @throw [NSException new];
    }
    v = *(UInt32*)[data bytes];
    return ntohl(v);
}

-(UInt64)popUint64BE
{
    UInt64 v;
    NSData *data = [self receiveDataOfSize:sizeof(v)];
    if (!data)
    {
        @throw [NSException new];
    }
    v = *(UInt64*)[data bytes];
    return ntohll(v);
}

-(float)popUintFloatBE
{
    UInt32 v = [self popUint32BE];
    return *(float*)&v;
}

-(double)popUintDoubleBE
{
    UInt64 v = [self popUint64BE];
    return *(double*)&v;
}

-(CFSocketNativeHandle)nativeHandle
{
    return CFSocketGetNative(_socket);
}

@end // @implementation TANTCPSocket

#pragma mark -
@implementation TANTCPSocket (Private)
-(BOOL)initSocketFromNativeHandle:(CFSocketNativeHandle)nativeHandle
{
    BOOL success = NO;
    
    CFSocketContext context;
    bzero(&context, sizeof(context));
    context.info = self;
    
    _socket = ::CFSocketCreateWithNative(kCFAllocatorDefault,
                               nativeHandle,
                                kCFSocketAcceptCallBack,
                               _socketCallBackWrapper,
                               &context);
    NSAssert(_socket, @"socket is nil");
    
    
    if (_socket)
    {
        if ([self addToRunLoop])
        {
            success = YES;
        }
    }
    
    return success;
}

-(BOOL)initSocket
{
    BOOL success = NO;
    
    CFSocketContext context;
    bzero(&context, sizeof(context));
    context.info = self;
    
    _socket = ::CFSocketCreate(kCFAllocatorDefault,
                               PF_INET,
                               SOCK_STREAM,
                               IPPROTO_TCP,
                               kCFSocketAcceptCallBack,
                               _socketCallBackWrapper,
                               &context);
    NSAssert(_socket, @"socket is nil");
    
    
    if (_socket)
    {
        if ([self addToRunLoop])
        {
            success = YES;
        }
    }
    
    return success;
}

-(void)removeFromRunLoop
{
    if (_runLoop && _runLoopSrc)
    {
        ::CFRunLoopRemoveSource(_runLoop, _runLoopSrc, kCFRunLoopCommonModes);
    }
}

-(BOOL)addToRunLoop
{
    NSAssert(_runLoop, @"No RunLoop?");
    if (_runLoop)
    {
        _runLoopSrc = ::CFSocketCreateRunLoopSource(kCFAllocatorDefault, _socket, 0);
        if (_runLoopSrc)
        {
            ::CFRunLoopAddSource(_runLoop, _runLoopSrc, kCFRunLoopCommonModes);
        }
    }
    
    return _runLoopSrc != nil;
}

-(BOOL)setReusePort
{
    // Set reuse port for bind
    int nativeSocket = [self nativeHandle];
    
    const int value = 1;
    int rc = ::setsockopt(nativeSocket, SOL_SOCKET, SO_REUSEPORT, &value, sizeof(value));
    return 0 == rc ;
}

-(void)handleEventForSocket:(CFSocketRef)s withCallbackType:(CFSocketCallBackType)type forAddress:(CFDataRef)address withData:(const void*)data
{}
@end // @implementation TANTCPSocket (Private)


#pragma mark -
#pragma mark Private stuff
static void _socketCallBackWrapper(CFSocketRef s, CFSocketCallBackType type, CFDataRef address, const void *data, void *info)
{
    TANTCPSocket *socketInstance = (TANTCPSocket*)info;
    if ([socketInstance methodForSelector:@selector(handleEventForSocket:withCallbackType:forAddress:withData:)])
    {
        [socketInstance handleEventForSocket:s withCallbackType:type forAddress:address withData:data];
    }
}

static CFDataRef dataForAddressAndPort(in_addr_t inAddress, in_port_t inPort)
{
    sockaddr_in sockAddr;
    bzero(&sockAddr, sizeof(sockAddr));
    
    sockAddr.sin_len = sizeof(sockAddr);
    sockAddr.sin_family = PF_INET;
    sockAddr.sin_port = htons(inPort);
    sockAddr.sin_addr.s_addr = htonl(inAddress);
    
    return (CFDataRef)[NSData dataWithBytes:&sockAddr length:sizeof(sockAddr)];
}

// Extension of ntohs, ntohl as ntohll
static inline uint64_t htonll(const uint64_t& inValue)
{
    return OSSwapHostToBigConstInt64(inValue);
}

static inline uint64_t ntohll(const uint64_t& inValue)
{
    return OSSwapBigToHostConstInt64(inValue);
}

