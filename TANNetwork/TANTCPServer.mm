/*
 File: TANTCPServer.mm
 
 Created by Dusel Hans-Peter on 07.08.11.
 
 License: Open Source Initiative OSI - The MIT License (MIT):Licensing The MIT License (MIT)
 http://www.opensource.org/licenses/MIT
 
 Copyright (c) 2011 Tangerine-Software, Hans-Peter Dusel, hdusel@tangerine-soft.de
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "TANTCPServer.h"
#import "TANTCPServerSocket.h"
enum ERunState {
      eStateStopped
    , eStateStarting
    , eStateRunning
    , eStateStopping
};

@implementation TANTCPServer

-(id)initWithPort:(in_port_t)inPort
{
    self = [super init];
    if (self) 
    {
        _port = inPort;
        _serverCondition = [NSCondition new];
        _state = eStateStopped;
    }
    return self;
}

- (void)dealloc 
{
    [_serverCondition release];
    [super dealloc];
}

-(void)setDelegate:(id<TANTCPServerDelegate>)inDelegate
{
    _delegate = inDelegate;
}

-(void)asyncFeeder:(id)inArg
{
    NSAutoreleasePool *arp = [NSAutoreleasePool new];
    
    [_serverCondition lock];

    _socket = [[TANTCPServerSocket alloc] initWithPort:_port];

    _state = eStateRunning;
    [_serverCondition signal];
    [_serverCondition unlock];

    CFRunLoopRun();

    [_serverCondition lock];
    _state = eStateStopped;
    [_serverCondition signal];
    [_serverCondition unlock];
    
    [arp release];
}

-(BOOL)start
{
    [_serverCondition lock];
    _state = eStateStarting;
    [_serverCondition signal];

    [NSThread detachNewThreadSelector:@selector(asyncFeeder:) toTarget:self withObject:nil];

    while ( eStateRunning != _state)
    {
        [_serverCondition wait];
    }
    [_serverCondition unlock];

    if (_socket && [_delegate respondsToSelector:@selector(startListenSocketOnServer:)])
    {
        [_delegate startListenSocketOnServer:self];
    }

    [_socket setDelegate:self];
    
    return _socket != nil;
}

-(BOOL)stop
{
    if (_socket)
    {
        [_socket setDelegate:nil];

        [_serverCondition lock];
        _state = eStateStopping;
        [_serverCondition unlock];
        
        if (_socket && [_delegate respondsToSelector:@selector(stopListenSocketOnServer:)])
        {
            [_delegate stopListenSocketOnServer:self];
        }
        
        [_socket disconnect];
        [_socket release];

        [_serverCondition lock];
        while ( eStateStopped != _state)
        {
            [_serverCondition wait];
        }
        [_serverCondition unlock];

        
        return YES;
    }
    else
    {
        return NO;
    }
}

-(BOOL)isRunning
{
    return _socket != nil;
}

#pragma mark TANTCPServerSocketProtocol
-(void)connectRequestOnSocket:(TANTCPSocket*)inSocket
{
    if (_delegate)
    {
        [_delegate connectRequestOnSocket:inSocket];
    }
}
@end // @implementation TANTCPServer

