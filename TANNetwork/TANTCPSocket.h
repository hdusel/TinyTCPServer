#pragma once
/*
 File: TANTCPSocket.h
 
 Created by Dusel Hans-Peter on 07.08.11.
 
 License: Open Source Initiative OSI - The MIT License (MIT):Licensing The MIT License (MIT)
 http://www.opensource.org/licenses/MIT
 
 Copyright (c) 2011 Tangerine-Software, Hans-Peter Dusel, hdusel@tangerine-soft.de
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import <CoreFoundation/CoreFoundation.h>
#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>

@class NSRunLoop;

@interface TANTCPSocket : NSObject
{
    CFSocketRef _socket;
    CFRunLoopRef _runLoop;
    CFRunLoopSourceRef _runLoopSrc;
}

-(id)init;
-(id)initWithNativeSocket:(CFSocketNativeHandle)nativeHandle;
-(void)disconnect;
-(BOOL)bindToIPAddress:(in_addr_t)inAddress withPort:(in_port_t)inPort;
-(BOOL)connectToPeer:(in_addr_t)inAddress usingPort:(in_port_t)inPort;
-(BOOL)isValid;

-(BOOL)sendData:(NSData*)data;
-(BOOL)sendString:(NSString*)string;

// -------
-(NSData*)receiveDataOfSize:(size_t)inSize;

-(void)pushUint8:(UInt8)inValue;
-(void)pushUint16BE:(UInt16)inValue;
-(void)pushUint32BE:(UInt32)inValue;
-(void)pushUint64BE:(UInt64)inValue;
-(void)pushFloatBE:(float)inValue;
-(void)pushDoubleBE:(double)inValue;

-(UInt8)popUint8;
-(UInt16)popUint16BE;
-(UInt32)popUint32BE;
-(UInt64)popUint64BE;
-(float)popUintFloatBE;
-(double)popUintDoubleBE;


@property (nonatomic, readonly, assign) CFSocketNativeHandle nativeHandle;

@end // @interface TANTCPSocket : NSObject

@interface TANTCPSocket(LengthEncodedExtension)
-(void)sendLengthEncodedString:(NSString*)inString;

/** Receive a "length encoded" string. This means that the peer has sent a string whose
 * length is introduced by a big endian encoded long (4 bytes).
 * A convenient method to do so is -[TANTCPSocket sendLengthEncodedString:]
 * \see -[TANTCPSocket sendLengthEncodedString:]
 */
-(NSString*)receiveLengthEncodedString;

-(void)sendLengthEncodedData:(NSData*)inData;
-(NSData*)receiveLengthEncodedData;

@end
