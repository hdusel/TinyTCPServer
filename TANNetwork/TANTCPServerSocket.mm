/*
 File: TANTCPServerSocket.h
 
 Created by Dusel Hans-Peter on 07.08.11.
 
 License: Open Source Initiative OSI - The MIT License (MIT):Licensing The MIT License (MIT)
 http://www.opensource.org/licenses/MIT
 
 Copyright (c) 2011 Tangerine-Software, Hans-Peter Dusel, hdusel@tangerine-soft.de
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#import "TANTCPSocket.h"
#import "TANTCPServerSocket.h"
#import <Foundation/Foundation.h>

@interface TANTCPServerSocket (Private)
@end // @interface TANTCPServerSocket (Private)

@implementation TANTCPServerSocket (Private)

@end // @implementation TANTCPServerSocket (Private)

@implementation TANTCPServerSocket
@synthesize port = _port;

-(void)setDelegate:(id<TANTCPServerSocketProtocol>)delegate
{
    _delegate = delegate;
}

-(id)initWithPort:(in_port_t)inPort
{
    self = [super init];
    if (self)
    {
        _port = inPort;

        if ( ! [self bindToIPAddress:INADDR_LOOPBACK withPort:inPort] )
        {
            [self release];
            self = nil;
        }
    }
    return self;
}

-(void)handleEventForSocket:(CFSocketRef)s
           withCallbackType:(CFSocketCallBackType)type
                 forAddress:(CFDataRef)address
                   withData:(const void*)data
{
    switch (type)
    {
        case kCFSocketAcceptCallBack:
            if (_delegate)
            {
                CFSocketNativeHandle& targetSocket = *((CFSocketNativeHandle*)data);
                
                TANTCPSocket *connectedSocket = [[[TANTCPSocket alloc] initWithNativeSocket:targetSocket] autorelease];
                
                [_delegate connectRequestOnSocket:connectedSocket];
            }
            break;
    }
    
}


@end // @implementation TANTCPSocket
